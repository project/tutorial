
/*
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    //kwresearch_init();

  });
}
*/
Drupal.behaviors.tutorial_init = function (context) {
  tutorial_init();
}

var tutorial_init = function () {
  var embeds = Drupal.settings.tutorial.embeds;
  var tutorials = Drupal.settings.tutorial.tutorials;
  var out = '<div id="tutorial-lists" class="tutorial-lists tutorial-tooltips">';
  var i=1;
  var img = '<img src="' + Drupal.settings.tutorial.path_to_module + '/images/icon_12x12.png" class="tutorial-icon" alt="Tutorial help">';
  for(var selector in embeds) {
	  var embed = embeds[selector];

	  var list = '<ol class="tutorial-list">';
	  var tid0 = '';
	  for(j=0; j < embed.length; j++) {
		  var e = embed[j];
		  if(tid0 == '') {
			tid0 = e['tid'];  
		  }
		  var t = tutorials[e['tid']];
		  list += '<li><a href="#" class="tutorial-'+e['tid']+' toollistitem" tutorial-link="tutorial-'+e['tid']+'">' + t['title'] + '</a> (' + t['length'] +  ')</li>';
	  }
	  list += '</ol>';
	  out += '<div id="tutorial-list-'+i+'" class="tutorial-list" style="">';
	  out += list;
	  //out += '<img src="http://img121.imageshack.us/img121/746/thailand.jpg" />';
	  out += '</div>';	  
  
	  var icon = '<a href="#" class="list icon tutorial-toggler" tutorial-list-link="tutorial-list-'+i+'" tutorial-link="tutorial-'+tid0+'">'+img+'</a>';
	  $(selector).append(icon);

	  i++;	  
  }
  out += '</div>';
  out += '<div id="tutorial-tutorial" class="tutorial-tutorial tutorial-tooltips">';
  for(var tid in tutorials) {
	  var tutorial = tutorials[tid];
	  out += '<div id="tutorial-'+tid+'" class="tutorial-tutorial" style="">';
	  out += '<div class="tutorial-header">';
	  out += tutorial['title'];
	  out += '<a href="#" class="tutorial-close">[x]</a>';
	  out += '</div>';
	  out += tutorial['body'];
	  out += '</div>';	  
  }    
  out += '</div>';
  $(document.body).append(out);
  stickytooltip.init();
}

var stickytooltip={
	tooltipoffsets: [10, -30], //additional x and y offset from mouse cursor for tooltips
	fadeinspeed: 200, //duration of fade effect in milliseconds
	tutorialactive: false,
	inlistbox: false,

	//***** NO NEED TO EDIT BEYOND HERE

	isdocked: false,

	positiontooltip:function($, $tooltip, e){
		var x=e.pageX+this.tooltipoffsets[0], y=e.pageY+this.tooltipoffsets[1]
		var tipw=$tooltip.outerWidth(), tiph=$tooltip.outerHeight(), 
		x=(x+tipw>$(document).scrollLeft()+$(window).width())? x-tipw-(stickytooltip.tooltipoffsets[0]*2) : x
		y=(y+tiph>$(document).scrollTop()+$(window).height())? $(document).scrollTop()+$(window).height()-tiph-10 : y
		$tooltip.css({left:x, top:y})
	},
	
	showbox:function($, $tooltip, doPosition, e){
	  $tooltip.fadeIn(this.fadeinspeed)
	  if(doPosition) {
	    this.positiontooltip($, $tooltip, e)
	  }
	},

	hidebox:function($, $tooltip){
		$tooltip.fadeOut(this.fadeinspeed);
	},

	init:function(){
		var $clicktargets = $('*[tutorial-link]');
		var $hovertargets = $('*[tutorial-list-link]');
		var $tooltip_lists=$('#tutorial-lists').appendTo(document.body)
		var $tooltip_tutorials=$('#tutorial-tutorials').appendTo(document.body)
		if (($clicktargets.length==0) && ($hovertargets.length==0)) {
		  return;
		}
		var $alllists=$tooltip_lists.find('div.tutorial-list');
		var $alltutorials=$tooltip_tutorials.find('div.tutorial-tutorial');
// $('#tutorial-tooltips').find('div.tutorial-list')
		stickytooltip.hidebox($, $tooltip_lists);
		stickytooltip.hidebox($, $tooltip_tutorials);
		$hovertargets.bind('mouseenter', function(e){
			$alltutorials.hide()
			$alllists.hide().filter('#'+$(this).attr('tutorial-list-link')).show()
			stickytooltip.showbox($, $tooltip_lists, true, e)
		})

		$hovertargets.bind('mouseleave', function(e){
		  if(!stickytooltip.tutorialactive || !stickytooltip.inlistbox) {
		    stickytooltip.hidebox($, $tooltip_lists)
		  }
		})
		$clicktargets.bind("click", function(e){
			stickytooltip.tutorialactive = true;	
			$alllists.hide();
			$alltutorials.hide().filter('#'+$(this).attr('tutorial-link')).show();
			stickytooltip.hidebox($, $tooltip_lists);
			stickytooltip.showbox($, $tooltip_tutorials, true, e);
			return false
		})
		$('.tutorial-close').bind("click", function(e){
			stickytooltip.hidebox($, $tooltip_tutorials);
			stickytooltip.tutorialactive = false;
			return false;
		})
		$alllists.bind("mouseenter", function(e){
			stickytooltip.inlistbox = true;
			stickytooltip.showbox($, $tooltip_lists, false, e);
		})
		$alllists.bind("mouseleave", function(e){
			stickytooltip.inlistbox = false;
			if(!stickytooltip.tutorialactive) {
			  stickytooltip.hidebox($, $tooltip_lists)
			}
		})
	}
}

//stickytooltip.init("targetElementSelector", "tooltipcontainer")
